module.exports = {
    "extends": [
        "airbnb-base", 
        "prettier"
    ],
    "env": {
        "browser": true,
        "node": true,
        "jest": true
    },
    "parserOptions": {
        "ecmaVersion": 8,
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "impliedStrict": true,
            "classes": true
        }
    },
    "rules": {
        "no-console": 1,
        "quotes": [
            2,
            "single",
            {
                "avoidEscape": true,
                "allowTemplateLiterals": true
            }
        ],
        "prettier/prettier": [
            "error",
            {
                "trailingComma": "none",
                "singleQuote": true,
                "printWidth": 100
            }
        ],
    },
    "plugins": [
        "prettier"
    ]
};